const getBoardData = require("./callback1");
const getListData = require("./callback2");
const getCardObject = require("./callback3");
const boardArray = require("./test/boards.json");
const listObject = require("./test/lists.json");

function getBoardDataAndAllCardData(boardName, cardObject) {
  if (
    boardName == undefined ||
    cardObject == undefined ||
    typeof boardName !== "string"
  ) {
    throw new Error("data is missing");
  }

  let boardID = findBoardId(boardName);

  if (boardID == undefined) {
    throw new Error("ID not found");
  } else {
    getBoardData(boardArray, boardID, (err1, boardObject) => {
      if (err1) {
        console.log(err1.message);
      } else {
        console.log(boardObject);
        getListData(listObject, boardID, (err2, listArray) => {
          if (err2) {
            console.log(err2.message);
          } else {
            console.log(listArray);
            for (let [boardID, listValue] of Object.entries(listObject)) {
              let wait = 0;
              for (let index = 0; index < listValue.length; index++) {
                getCardObject(
                  cardObject,
                  listValue[index].id,
                  (err3, cardArray) => {
                    if (err3) {
                      console.log(err3.message);
                    } else {
                      let offset = 0;
                      cardArray.forEach((card) => {
                        setTimeout(() => {
                          console.log(card);
                        }, 2000 + offset + wait);
                        offset += 2000;
                        wait += 2000;
                      });
                    }
                  }
                );
              }
            }
          }
        });
      }
    });
  }
}

function findBoardId(boardName) {
  const object = boardArray.find((board) => board.name == boardName);
  return object.id;
}

module.exports = getBoardDataAndAllCardData;
