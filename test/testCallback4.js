const getBoardDataAndListData = require("../callback4");

// when data passed is correct
try {
  getBoardDataAndListData("Thanos", "Mind");
} catch (error) {
  console.log(error.message);
}

// when data passed is incorrect
try {
  getBoardDataAndListData(null);
} catch (error) {
  console.log(error.message);
}

// when data passes is array
try {
  getBoardDataAndListData([],[]);
} catch (error) {
  console.log(error.message);
}