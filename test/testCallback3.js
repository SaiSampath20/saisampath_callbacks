const getCardObject = require("../callback3");
const cardObject = require("./cards.json");

// when data is correct
try {
  getCardObject(cardObject, "qwsa221", (err, cardArray) => {
    if (err) {
      console.log(err.message);
    } else {
      console.log(cardArray);
    }
  });
} catch (error) {
  console.log(error.message);
}

// when data is incorrect
try {
  getCardObject(cardObject, null, (err, cardArray) => {
    if (err) {
      console.log(err.message);
    } else {
      console.log(cardArray);
    }
  });
} catch (error) {
  console.log(error.message);
}

// instead of object passing array
try {
  getCardObject([], "cffv432", (err, cardArray) => {
    if (err) {
      console.log(err.message);
    } else {
      console.log(cardArray);
    }
  });
} catch (error) {
  console.log(error);
}
