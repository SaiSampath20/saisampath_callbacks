const getBoardDataAndAllCardData = require("../callback6");
const cardObject = require("./cards.json");

// when data passed is correct
try {
  getBoardDataAndAllCardData("Thanos", cardObject);
} catch (error) {
  console.log(error.message);
}

// when data passed is incorrect
try {
  getBoardDataAndAllCardData(null, null);
} catch (error) {
  console.log(error.message);
}

// when data passed is array 
try {
  getBoardDataAndAllCardData([], []);
} catch (error) {
  console.log(error.message);
}