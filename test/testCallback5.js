const getBoardDataListData = require("../callback5");

// when data passed is correct
try {
  getBoardDataListData("Thanos", "Mind", "Space");
} catch (error) {
  console.log(error.message);
}

// when data passed is incorrect
try {
  getBoardDataListData("Thanos", null, "Space");
} catch (error) {
  console.log(error.message);
}

// when data passed is not string
try {
  getBoardDataListData([], [], []);
} catch (error) {
  console.log(error.message);
}