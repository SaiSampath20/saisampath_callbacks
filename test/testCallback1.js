const fs = require("fs");
const getBoardData = require("../callback1");

// when data is correct
fs.readFile("./test/boards.json", "utf8", (err, boardArray) => {
  if (err) {
    console.log(err.message);
  } else {
    try {
      getBoardData(JSON.parse(boardArray), "mcu453ed", (err, boardObject) => {
        if (err) {
          console.log(err.message);
        } else {
          console.log(boardObject);
        }
      });
    } catch (error) {
      console.log(error.message);
    }
  }
});

// when data is incorrect
fs.readFile("./test/boards.json", "utf8", (err, boardArray) => {
  if (err) {
    console.log(err.message);
  } else {
    try {
      getBoardData(JSON.parse(boardArray), null, (err, boardObject) => {
        if (err) {
          console.log(err.message);
        } else {
          console.log(boardObject);
        }
      });
    } catch (error) {
      console.log(error.message);
    }
  }
});

// when callback function is not passed
fs.readFile("./test/boards.json", "utf8", (err, boardArray) => {
  if (err) {
    console.log(err.message);
  } else {
    try {
      getBoardData(JSON.parse(boardArray), null);
    } catch (error) {
      console.log(error.message);
    }
  }
});
