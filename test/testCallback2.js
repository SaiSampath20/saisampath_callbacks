const getListData = require("../callback2");
const listObject = require("./lists.json");

// when data is correct
try {
  getListData(listObject, "mcu453ed", (err, listArray) => {
    if (err) {
      console.log(err.message);
    } else {
      console.log(listArray);
    }
  });
} catch (error) {
  console.log(error.message);
}

// when data is incorrect
try {
  getListData(listObject, "abc122d", (err, listArray) => {
    if (err) {
      console.log(err.message);
    } else {
      console.log(listArray);
    }
  });
} catch (error) {
  console.log(error.message);
}
