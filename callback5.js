const getBoardData = require("./callback1");
const getListData = require("./callback2");
const getCardObject = require("./callback3");
const boardArray = require("./test/boards.json");
const listObject = require("./test/lists.json");
const cardObject = require("./test/cards.json");

function getBoardDataListData(boardName, listName1, listName2) {
  if (
    boardName == undefined ||
    listName1 == undefined ||
    listName2 == undefined ||
    typeof boardName !== "string" ||
    typeof listName1 !== "string" ||
    typeof listName2 !== "string"
  ) {
    throw new Error("data is missing");
  }

  let boardID = findBoardId(boardName);
  let listID1 = findListId(listName1);
  let listID2 = findListId(listName2);

  if (boardID == undefined || listID1 == undefined || listID2 == undefined) {
    throw new Error("ID not found");
  } else {
    getBoardData(boardArray, boardID, (err1, boardObject) => {
      if (err1) {
        console.log(err1.message);
      } else {
        console.log(boardObject);
        getListData(listObject, boardID, (err2, listArray) => {
          if (err2) {
            console.log(err2.message);
          } else {
            console.log(listArray);
            getCardObject(cardObject, listID1, (err3, cardArray1) => {
              if (err3) {
                console.log(err3.message);
              } else {
                let offset1 = 0;
                cardArray1.forEach((card) => {
                  setTimeout(() => {
                    console.log(card);
                  }, 2000 + offset1);
                  offset1 += 2000;
                });
                getCardObject(cardObject, listID2, (err4, cardArray2) => {
                  if (err4) {
                    console.log(err4.message);
                  } else {
                    let offset2 = 0;
                    cardArray2.forEach((card) => {
                      setTimeout(() => {
                        console.log(card);
                      }, 2000 + offset2);
                      offset2 += 2000;
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }
}

function findBoardId(boardName) {
  const object = boardArray.find((board) => board.name == boardName);
  return object.id;
}

function findListId(listName) {
  let listID = "";
  for (const [key, value] of Object.entries(listObject)) {
    let object = value.find((element) => element.name == listName);
    if (object != undefined) {
      listID = object.id;
      break;
    }
  }
  return listID;
}

module.exports = getBoardDataListData;
