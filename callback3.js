function getCardObject(cardObject, listID, callback) {
  if (
    arguments.length > 3 ||
    arguments.length < 3 ||
    typeof cardObject !== "object"
  ) {
    throw new Error("data is missing");
  }
  setTimeout(() => {
    try {
      const card = cardObject[listID];
      if (card != undefined) {
        callback(null, card);
      } else {
        throw new Error("data not found");
      }
    } catch (error) {
      console.log(error.message);
    }
  }, 2 * 1000);
}

module.exports = getCardObject;
