const getBoardData = require("./callback1");
const getListData = require("./callback2");
const getCardObject = require("./callback3");
const boardArray = require("./test/boards.json");
const listObject = require("./test/lists.json");
const cardObject = require("./test/cards.json");

function getBoardDataAndListData(boardName, listName) {
  if (
    arguments.length > 2 ||
    arguments.length < 2 ||
    typeof boardName !== "string" ||
    typeof listName !== "string"
  ) {
    throw new Error("data is missing");
  }

  let boardID = findBoardId(boardName);
  let listID = findListId(listName);

  if (boardID == undefined || listID == undefined) {
    throw new Error("ID not found");
  } else {
    getBoardData(boardArray, boardID, (err, boardObject) => {
      if (err) {
        console.log(err.message);
      } else {
        console.log(boardObject);
        getListData(listObject, boardID, (err1, listArray) => {
          if (err1) {
            console.log(err1.message);
          } else {
            console.log(listArray);
            getCardObject(cardObject, listID, (err2, cardArray) => {
              if (err2) {
                console.log(err2.message);
              } else {
                let offset = 0;
                cardArray.forEach((card) => {
                  setTimeout(() => {
                    console.log(card);
                  }, 2000 + offset);
                  offset += 2000;
                });
              }
            });
          }
        });
      }
    });
  }
}

function findBoardId(boardName) {
  const object = boardArray.find((board) => board.name == boardName);
  return object.id;
}

function findListId(listName) {
  let listID = "";
  for (const [key, value] of Object.entries(listObject)) {
    let object = value.find((element) => element.name == listName);
    if (object != undefined) {
      listID = object.id;
      break;
    }
  }
  return listID;
}

module.exports = getBoardDataAndListData;
