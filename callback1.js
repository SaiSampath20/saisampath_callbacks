function getBoardData(boardArray, boardID, callback) {
  if (
    arguments.length > 3 ||
    arguments.length < 3 ||
    !Array.isArray(boardArray)
  ) {
    throw new Error("data is missing");
  }
  setTimeout(() => {
    const boardInfo = boardArray.find((board) => board.id == boardID);
    try {
      if (boardInfo == undefined) {
        throw new Error("data not found");
      } else {
        callback(null, boardInfo);
      }
    } catch (error) {
      console.log(error.message);
    }
  }, 2 * 1000);
}

module.exports = getBoardData;
