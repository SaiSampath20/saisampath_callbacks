function getListData(listObject, boardID, callback) {
  if (
    arguments.length > 3 ||
    arguments.length < 3 ||
    typeof listObject !== "object"
  ) {
    throw new Error("data is missing");
  }
  setTimeout(() => {
    try {
      const list = listObject[boardID];
      if (list != undefined) {
        callback(null, list);
      } else {
        throw new Error("data not found");
      }
    } catch (error) {
      console.log(error.message);
    }
  }, 2 * 1000);
}

module.exports = getListData;
